package com.mbayennapp.senautodriver.Helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.mbayennapp.senautodriver.Model.Demandes;

/**
 * Created by Mbaye on 30/10/2017.
 */

public class SessionManagerDemande {
    private static String TAG = SessionManagerDemande.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Shared preferences file name
    private static final String PREF_NAME = "DemandeDetails";
    private static final String KEY_IS_LOGGEDIN = "statutDemande";

    public SessionManagerDemande(Context _context) {
        this._context = _context;
        pref = _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
    }
    public void storeDemandeDetail(Demandes dem){
        editor = pref.edit();
        editor.putString("key",dem.getKEY());
        editor.putString("adresseDep",dem.getAdresseDepart());
        editor.putString("adresseArr",dem.getAdresseArrivee());
        editor.putString("gpsDep",dem.getGpsDepart());
        editor.putString("gpsArr",dem.getGpsArrivee());
        editor.putString("date",dem.getDate());
        editor.putString("distance",dem.getDistance());
        editor.putString("duree",dem.getDuree());
        editor.putString("heure",dem.getHeure());
        editor.putString("etat","Demande accepted");
        editor.commit();
    }

    public Demandes getStoreDemandeDetail(){
        String key = pref.getString("key","");
        String adresseDep = pref.getString("adresseDep","");
        String adresseArr = pref.getString("adresseArr","");
        String gpsDep = pref.getString("gpsDep","");
        String gpsArr = pref.getString("gpsArr","");
        String date = pref.getString("date","");
        String distance = pref.getString("distance","");
        String duree = pref.getString("duree","");
        String heure = pref.getString("heure","");
        String etat = pref.getString("etat","");
        Demandes dem = new Demandes(key,adresseDep,adresseArr,gpsDep,gpsArr,date,distance,duree,heure,etat);
        return dem;
    }

    public void manageEtat(String etat){
        editor = pref.edit();
        editor.putString("etat",etat);
        editor.commit();
    }

    public void setLoggedInClientLogin(boolean loggedIn){
        editor = pref.edit();
        editor.putBoolean("LoggedIn",loggedIn);
        editor.commit();
    }
    public void clearClientLoginData(){
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }
    public boolean isLoggedIn(){
        Demandes dem = getStoreDemandeDetail();
        String key = dem.getKEY();
        boolean statut = pref.getBoolean(KEY_IS_LOGGEDIN, false);
        if (key != ""){
            statut = pref.getBoolean(KEY_IS_LOGGEDIN, true);
        }
        return statut;
    }
}
