package com.mbayennapp.senautodriver.Modules;

/**
 * Created by Mbaye on 18/03/2017.
 */

public class Distance {
    public String text;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
