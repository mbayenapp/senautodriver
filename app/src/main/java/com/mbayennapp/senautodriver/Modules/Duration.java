package com.mbayennapp.senautodriver.Modules;

/**
 * Created by Mbaye on 18/03/2017.
 */

public class Duration {
    public String text;
    public int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
