package com.mbayennapp.senautodriver.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mbayennapp.senautodriver.Helper.SessionManagerDemande;
import com.mbayennapp.senautodriver.Model.Demandes;
import com.mbayennapp.senautodriver.Modules.DirectionFinder;
import com.mbayennapp.senautodriver.Modules.DirectionFinderListener;
import com.mbayennapp.senautodriver.Modules.Route;
import com.mbayennapp.senautodriver.R;
import com.mbayennapp.senautodriver.Utils.GPSTracker;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener, DirectionFinderListener {
    private static final String TAG = Home.class.getSimpleName();
    private GoogleMap mMap;
    private double lattitude = 0.0, longitude = 0.0;
    private GPSTracker mGPS;
    private String KEY = "AIzaSyAUkyEG9duEFiVRqtWc3U8iHqNZDopIlQI";
    private String URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=";
    /*========================================================================*/
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseUser user;
    private List<Demandes> demandes = new ArrayList<>();
    private Demandes demande;
    private Demandes dem_init;
    /*=============================================================*/
    private TextView txt_new_request, txt_name_custumer, txt_tel_custumer;
    private Button btn_accept_request, btn_denied_request,
            btn_driver_arrived, btn_destination, btn_deposed;
    private RelativeLayout r_sect_show_request, r_my_loader, r_sect_request_accepted;
    /*=============================================================*/
    private Handler myHandler;
    private DatabaseReference d_chauffeur;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;
    private LatLng post_chauffeur = null;
    private int init_new_demande = 1;
    private Location locA, locB;
    private float distance_beetwen = 0;
    private int init_dem_start = 0;
    private String key_dem = "";
    private Animation animSideUp, animSideRoll, animSlideUp, animSlideDown, animRollDemande, animSideFadeOut, animFadeIn;
    DatabaseReference new_request;
    /*==============================================================*/
    private SessionManagerDemande sessionManagerDemande;
    private String prevlat = "", prevlng = "";
    /*==============================================================*/
    private Polyline polylinePaths = null;
    private Marker originMarkers = null;
    private Marker destinationMarkers = null;
    private PolylineOptions polylineOptions;
    private float bearing = (float) 0.00;
    /*==============================================================*/
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    /*==============================================================*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        /*=================================================================================*/
        initAnimation();
        rollAnimation();
        SlideUpAnimation();
        SlideDownAnimation();
        RollDemandeAnimation();
        fadeOutAnime();
        fadeinAnime();
        txt_new_request = (TextView) findViewById(R.id.txtadresseDepart);
        btn_accept_request = (Button) findViewById(R.id.btn_accept_request);
        btn_denied_request = (Button) findViewById(R.id.btn_denied_request);
        r_sect_show_request = (RelativeLayout) findViewById(R.id.r_sect_show_request);
        r_sect_request_accepted = (RelativeLayout) findViewById(R.id.r_sect_request_accepted);
        r_my_loader = (RelativeLayout) findViewById(R.id.r_my_loader);
        txt_name_custumer = (TextView) findViewById(R.id.txt_name_custumer);
        txt_tel_custumer = (TextView) findViewById(R.id.txt_tel_custumer);
        btn_driver_arrived = (Button) findViewById(R.id.btn_driver_arrived);
        btn_destination = (Button) findViewById(R.id.btn_destination);
        btn_deposed = (Button) findViewById(R.id.btn_deposed);
        /*=================================================================================*/
        sessionManagerDemande = new SessionManagerDemande(this);
        /*=================================================================================*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        /*=======================================================*/
        if (isNetworkAvailable(Home.this)) {
            mGPS = new GPSTracker(this);
        }
        final int permissionCheeck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permissionCheeck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
         /*==================================================================*/
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        /*=================================================================================*/
        mAuth = FirebaseAuth.getInstance();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = FirebaseAuth.getInstance().getCurrentUser();
            }
        };
        d_chauffeur = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Chauffeurs").child(mAuth.getCurrentUser().getUid());
        /*=================================================================================*/
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_home);
        mapFragment.getMapAsync(this);

        /*==================================================================================*/
        if (sessionManagerDemande.isLoggedIn()) {
            //Les informations du client
            String[] pt = sessionManagerDemande.getStoreDemandeDetail().getKEY().split("_");
            DatabaseReference dt_client = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Clients").child(pt[0]);
            dt_client.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        txt_name_custumer.setText(dataSnapshot.child("prenom").getValue().toString() + " " + dataSnapshot.child("nom").getValue().toString());
                        txt_tel_custumer.setText(dataSnapshot.child("tel").getValue().toString());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        btn_driver_arrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                r_my_loader.setVisibility(View.VISIBLE);
                String[] pt = sessionManagerDemande.getStoreDemandeDetail().getKEY().split("_");
                DatabaseReference dt_courses = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(sessionManagerDemande.getStoreDemandeDetail().getKEY());
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("Etat", "Driver arrived");
                dt_courses.updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            sessionManagerDemande.manageEtat("Driver arrived");
                            r_my_loader.setVisibility(View.GONE);
                            btn_driver_arrived.setVisibility(View.GONE);
                            btn_destination.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
        btn_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                r_my_loader.setVisibility(View.VISIBLE);
                String[] pt = sessionManagerDemande.getStoreDemandeDetail().getKEY().split("_");
                DatabaseReference dt_courses = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(sessionManagerDemande.getStoreDemandeDetail().getKEY());
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("Etat", "Custumer at board");
                dt_courses.updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            sessionManagerDemande.manageEtat("Custumer at board");
                            r_my_loader.setVisibility(View.GONE);
                            btn_driver_arrived.setVisibility(View.GONE);
                            btn_destination.setVisibility(View.GONE);
                            btn_deposed.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
        btn_deposed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                r_my_loader.setVisibility(View.VISIBLE);
                String[] pt = sessionManagerDemande.getStoreDemandeDetail().getKEY().split("_");
                DatabaseReference dt_courses = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(sessionManagerDemande.getStoreDemandeDetail().getKEY());
                HashMap<String, Object> hashMap = new HashMap<>();
                hashMap.put("Etat", "Custumer deposed");
                dt_courses.updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            sessionManagerDemande.manageEtat("Custumer deposed");
                            r_my_loader.setVisibility(View.GONE);
                            btn_driver_arrived.setVisibility(View.GONE);
                            btn_destination.setVisibility(View.GONE);
                            btn_deposed.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        });
        /*==================================================================================*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json);
        mMap.setMapStyle(style);
        if (isNetworkAvailable(Home.this)) {
            mGPS = new GPSTracker(this);
            if (mGPS.canGetLocation) {
                mGPS.getLocation();
                lattitude = mGPS.getLatitude();
                longitude = mGPS.getLongitude();

                //Mise à jour de la position du taxi
                /*HashMap<String, Object> resPosition = new HashMap<>();
                resPosition.put("Lattitude",lattitude);
                resPosition.put("Longitude",longitude);
                d_chauffeur.updateChildren(resPosition);*/
                post_chauffeur = new LatLng(lattitude, longitude);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(post_chauffeur, 14));
                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.setBuildingsEnabled(true);
                mMap.setMinZoomPreference(6.0f);
                mMap.setMaxZoomPreference(18.0f);
            }
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    /*======================================================*/
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthStateListener);

        myHandler = new Handler();
        myHandler.postDelayed(myRunnableble, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(mAuthStateListener);
        if (myHandler != null) {
            myHandler.removeCallbacks(myRunnableble);
        }
    }

    /*======================================================*/
    public static boolean isNetworkAvailable(Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    /*==================================================================*/
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new CountDownTimer(1000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                        }

                        @Override
                        public void onFinish() {
                            if (mGPS.canGetLocation) {
                                mGPS.getLocation();
                                lattitude = mGPS.getLatitude();
                                longitude = mGPS.getLongitude();
                            }
                        }
                    }.start();
                } else {
                    finish();
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        d_chauffeur = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Chauffeurs").child(mAuth.getCurrentUser().getUid());
        d_chauffeur.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    prevlat = dataSnapshot.child("Lattitude").getValue().toString();
                    prevlng = dataSnapshot.child("Longitude").getValue().toString();
                    HashMap<String, Object> resPosition = new HashMap<>();
                    resPosition.put("Lattitude", lattitude);
                    resPosition.put("Longitude", longitude);
                    resPosition.put("prevlat", prevlat);
                    resPosition.put("prevlng", prevlng);
                    d_chauffeur.updateChildren(resPosition);
                    /*==========================================================*/
                    if (sessionManagerDemande.isLoggedIn()) {
                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mMap.setMyLocationEnabled(false);
                        String[] ptse = sessionManagerDemande.getStoreDemandeDetail().getGpsDepart().split(",");
                        LatLng endloc = null;
                        if (sessionManagerDemande.getStoreDemandeDetail().getEtat().equals("Demande accepted")){
                            endloc = new LatLng(Double.parseDouble(ptse[0]),Double.parseDouble(ptse[1]));
                        }
                        LatLng pos = new LatLng(lattitude, longitude);
                        mMap.clear();
                        Location prevLoc = new Location("prevloc");
                        prevLoc.setLatitude(Double.parseDouble(prevlat));
                        prevLoc.setLongitude(Double.parseDouble(prevlng));
                        Location newloc = new Location("newloc");
                        newloc.setLatitude(lattitude);
                        newloc.setLongitude(longitude);
                        bearing = prevLoc.bearingTo(newloc);
                        /*mMap.addMarker(new MarkerOptions()
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.mipmap.taxim_map))
                                .position(pos)
                                .anchor(0.6f, 0.6f)
                                .rotation(bearing)
                                .flat(true));*/
                        if (sessionManagerDemande.getStoreDemandeDetail().getEtat().equals("Demande accepted")) {
                            sendRequest(lattitude+","+longitude,sessionManagerDemande.getStoreDemandeDetail().getGpsDepart());
                        }else if (sessionManagerDemande.getStoreDemandeDetail().getEtat().equals("Custumer at board")){
                            sendRequest(lattitude+","+longitude,sessionManagerDemande.getStoreDemandeDetail().getGpsArrivee());
                        }

                    }
                    /*=========================================================*/
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    protected synchronized void buildGoogleApiClient(){
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
    /*==================================================================*/
    private Runnable myRunnableble = new Runnable() {
        @Override
        public void run() {
            /*==========================================================*/
            //Mettre à jour les coordonnées du chauffeur
            if (isNetworkAvailable(Home.this)){
                if (mGPS.canGetLocation) {
                    mGPS.getLocation();
                    lattitude = mGPS.getLatitude();
                    longitude = mGPS.getLongitude();
                    //Mise à jour de la position du taxi
                    /*d_chauffeur = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Chauffeurs").child(mAuth.getCurrentUser().getUid());
                    d_chauffeur.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                prevlat = dataSnapshot.child("Lattitude").getValue().toString();
                                prevlng = dataSnapshot.child("Longitude").getValue().toString();
                                HashMap<String, Object> resPosition = new HashMap<>();
                                resPosition.put("Lattitude", lattitude);
                                resPosition.put("Longitude", longitude);
                                resPosition.put("prevlat", prevlat);
                                resPosition.put("prevlng", prevlng);
                                d_chauffeur.updateChildren(resPosition);
                                *//*==========================================================*//*
                                mMap.clear();
                                Location prevLoc = new Location("prevloc");
                                prevLoc.setLatitude(Double.parseDouble(prevlat));
                                prevLoc.setLongitude(Double.parseDouble(prevlng));
                                Location newloc = new Location("newloc");
                                newloc.setLatitude(lattitude);
                                newloc.setLongitude(longitude);
                                float bearing = prevLoc.bearingTo(newloc);
                                LatLng pos = new LatLng(lattitude,longitude);
                                mMap.addMarker(new MarkerOptions()
                                        .icon(BitmapDescriptorFactory
                                                .fromResource(R.mipmap.taxim_map))
                                        .position(pos)
                                        .anchor(0.5f, 0.5f)
                                        .rotation(bearing)
                                        .flat(true));
                                *//*=========================================================*//*
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });*/
                }
            }
            /*==========================================================*/
            new_request = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes");
            new_request.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //Iterable<DataSnapshot>
                    if (dataSnapshot.exists()){
                        Log.d(TAG,"Total demande: "+dataSnapshot.getChildrenCount());
                        for (DataSnapshot ds : dataSnapshot.getChildren()){
                                if (ds.child("Etat").getValue(String.class).equals("Nouvelle demande")) {
                                    Log.d(TAG, "demande: " + ds.toString());
                                    if (init_new_demande <= dataSnapshot.getChildrenCount()) {
                                        demande = new Demandes();
                                        demande.setKEY(ds.getKey());
                                        demande.setAdresseArrivee(ds.child("AdresseArrivee").getValue(String.class));
                                        demande.setAdresseDepart(ds.child("AdresseDepart").getValue(String.class));
                                        demande.setDate(ds.child("Date").getValue(String.class));
                                        demande.setDistance(ds.child("Distance").getValue(String.class));
                                        demande.setDuree(ds.child("Duree").getValue(String.class));
                                        demande.setEtat(ds.child("Etat").getValue(String.class));
                                        demande.setGpsArrivee(ds.child("GpsArrivee").getValue(String.class));
                                        demande.setGpsDepart(ds.child("GpsDepart").getValue(String.class));
                                        demande.setHeure(ds.child("Heure").getValue(String.class));
                                        demande.setLattitude(String.valueOf(ds.child("Lattitude").getValue()));
                                        demande.setLongitude(String.valueOf(ds.child("Longitude").getValue()));
                                        demande.setType(ds.child("Type").getValue(String.class));
                                        demandes.add(demande);
                                        init_new_demande += 1;
                                    }
                                }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            Log.d(TAG,"Nbr New Demande: "+demandes.size());
            if (demandes.size() != 0) {
                for (int i = 0; i < demandes.size(); i++) {
                    locA = new Location("Post demande");
                    locA.setLatitude(lattitude);
                    locA.setLongitude(longitude);
                    locB = new Location("Post taxi");
                    String[] po = demandes.get(i).getGpsDepart().split(",");
                    locB.setLatitude(Double.parseDouble(po[0]));
                    locB.setLongitude(Double.parseDouble(po[1]));
                    distance_beetwen = locA.distanceTo(locB);
                    if (distance_beetwen <= 5000.00) {
                        if (init_dem_start == 0){
                            Log.d(TAG,"KEY _demande: "+demandes.get(i).getKEY());
                            Log.d(TAG,"Demande: "+demandes.get(i).getAdresseDepart());
                            dem_init = demandes.get(i);
                            //
                            init_dem_start= 1;
                            if (dem_init != null) {
                                txt_new_request.setText(dem_init.getAdresseDepart());
                                r_sect_show_request.setVisibility(View.VISIBLE);
                                r_sect_show_request.startAnimation(animFadeIn);
                                final DatabaseReference d_init = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Demandes").child(dem_init.getKEY());
                                final HashMap<String,Object> map_dem = new HashMap<>();
                                map_dem.put("Etat","Demande accepted");
                                btn_accept_request.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Calendar now = Calendar.getInstance();
                                        String[] p = now.getTime().toString().split(" ");
                                        String day = p[2];
                                        String month = p[1];
                                        String[] time = p[3].split(":");
                                        String heure = time[0];
                                        String minute = time[1];
                                        String annee = p[5];
                                        String mois = formatMois(month);

                                        final String date = day+"/"+mois+"/"+annee;
                                        final String temps = heure+":"+minute;
                                        r_my_loader.setVisibility(View.VISIBLE);
                                        r_sect_show_request.setVisibility(View.GONE);
                                        new CountDownTimer(1000,500){
                                            @Override
                                            public void onTick(long l) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                d_init.updateChildren(map_dem).addOnCompleteListener(Home.this, new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()){
                                                            final DatabaseReference d_inc = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Chauffeurs").child(mAuth.getCurrentUser().getUid());
                                                            d_inc.addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                                    final int inc = Integer.parseInt(dataSnapshot.child("increment").getValue().toString()) + 1;
                                                                    HashMap<String, Object> res = new HashMap<>();
                                                                    res.put("increment",String.valueOf(inc));
                                                                    d_inc.updateChildren(res).addOnCompleteListener(Home.this, new OnCompleteListener<Void>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<Void> task) {
                                                                            if (task.isSuccessful()){
                                                                                sessionManagerDemande.storeDemandeDetail(dem_init);
                                                                                DatabaseReference add_course = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Courses").child(d_init.getKey());
                                                                                add_course.child("chauffeur_id").setValue(mAuth.getCurrentUser().getUid());
                                                                                add_course.child("Date").setValue(date);
                                                                                add_course.child("Heure").setValue(temps);
                                                                                add_course.child("AdresseDepart").setValue(dem_init.getAdresseDepart());
                                                                                add_course.child("GpsDepart").setValue(dem_init.getGpsDepart());
                                                                                add_course.child("AdresseArrivee").setValue(dem_init.getAdresseArrivee());
                                                                                add_course.child("GpsArrivee").setValue(dem_init.getGpsArrivee());
                                                                                add_course.child("Distance").setValue(dem_init.getDistance());
                                                                                add_course.child("Etat").setValue("En cours");
                                                                                r_sect_request_accepted.setVisibility(View.VISIBLE);
                                                                                r_my_loader.setVisibility(View.GONE);
                                                                            }
                                                                        }
                                                                    });
                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {

                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }.start();
                                    }
                                });
                            }
                        }
                    }
                }
            }
            if (sessionManagerDemande.isLoggedIn()){
                if (sessionManagerDemande.getStoreDemandeDetail().getEtat().equals("Demande accepted")){
                    r_sect_show_request.setVisibility(View.GONE);
                    r_sect_request_accepted.setVisibility(View.VISIBLE);
                }else if (sessionManagerDemande.getStoreDemandeDetail().getEtat().equals("Driver arrived")){
                    r_sect_show_request.setVisibility(View.GONE);
                    r_sect_request_accepted.setVisibility(View.VISIBLE);
                    btn_driver_arrived.setVisibility(View.GONE);
                    btn_destination.setVisibility(View.VISIBLE);
                }else if (sessionManagerDemande.getStoreDemandeDetail().getEtat().equals("Custumer at board")){
                    r_sect_show_request.setVisibility(View.GONE);
                    r_sect_request_accepted.setVisibility(View.VISIBLE);
                    btn_driver_arrived.setVisibility(View.GONE);
                    btn_destination.setVisibility(View.GONE);
                    btn_deposed.setVisibility(View.VISIBLE);
                    btn_denied_request.setEnabled(false);
                    btn_denied_request.setTextColor(Color.parseColor("#171715"));
                    btn_denied_request.setBackgroundColor(Color.parseColor("#757575"));
                }else if (sessionManagerDemande.getStoreDemandeDetail().getEtat().equals("Custumer deposed")){
                    if (myHandler != null) {
                        myHandler.removeCallbacks(myRunnableble);
                    }
                    r_sect_show_request.setVisibility(View.GONE);
                    r_sect_request_accepted.setVisibility(View.GONE);
                    r_my_loader.setVisibility(View.VISIBLE);
                    new CountDownTimer(1500,500){
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            DatabaseReference dt_course = FirebaseDatabase.getInstance().getReference().child("Taxis").child("Courses").child(sessionManagerDemande.getStoreDemandeDetail().getKEY());
                            HashMap<String, Object> hashMap = new HashMap<String, Object>();
                            hashMap.put("Etat","closed");
                            dt_course.updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    sessionManagerDemande.clearClientLoginData();
                                    startActivity(new Intent(getApplicationContext(),Home.class));
                                    finish();
                                }
                            });
                        }
                    }.start();
                }
            }

            myHandler.postDelayed(this,1000);
        }
    };

    //Mes méthodes
    private String formatMois(String month){
        String mois = "";
        if (month.equals("Jan")){
            mois = "01";
        }else if (month.equals("Fev")){
            mois = "02";
        }else if (month.equals("Mar")){
            mois = "03";
        }else if (month.equals("Apr")){
            mois = "04";
        }else if (month.equals("May")){
            mois = "05";
        }else if (month.equals("Jun")){
            mois = "06";
        }else if (month.equals("Jul")){
            mois = "07";
        }else if (month.equals("Aug")){
            mois = "08";
        }else if (month.equals("Sep")){
            mois = "09";
        }else if (month.equals("Oct")){
            mois = "10";
        }else if (month.equals("Nov")){
            mois = "11";
        }else if (month.equals("Dec")){
            mois = "12";
        }
        return mois;
    }
    /*============================================================*/
    private void initAnimation(){
        animSideUp = AnimationUtils.loadAnimation(this,R.anim.pump_top);
        animSideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void rollAnimation(){
        animSideRoll = AnimationUtils.loadAnimation(this,R.anim.pump_bottom);
        animSideRoll.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void SlideUpAnimation(){
        animSlideUp = AnimationUtils.loadAnimation(this,R.anim.grow_from_bottom);
        animSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void SlideDownAnimation(){
        animSlideDown = AnimationUtils.loadAnimation(this,R.anim.disappear);
        animSlideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void RollDemandeAnimation(){
        animRollDemande = AnimationUtils.loadAnimation(this,R.anim.disappear);
        animRollDemande.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /*======================================================*/
    //Mes animation
    private void fadeOutAnime(){
        animSideFadeOut = AnimationUtils.loadAnimation(this,R.anim.outgoing);
        animSideFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    private void fadeinAnime(){
        animFadeIn = AnimationUtils.loadAnimation(this,R.anim.incoming);
        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(8000);
        mLocationRequest.setFastestInterval(8000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.disconnect();
    }

    private void sendRequest(String depart, String arrivee) {
        try {
            new DirectionFinder((DirectionFinderListener) Home.this, depart, arrivee).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onDirectionFinderStart() {
        if (polylinePaths != null) {
            polylinePaths.remove();
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        if (!routes.isEmpty()){
            for (Route route : routes) {
                originMarkers = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.mipmap.taxim_map)).
                                position(route.startLocation).flat(true).anchor((float) 0.5, (float) 0.5).rotation(bearing));
                destinationMarkers = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.pin_destination)).position(route.endLocation));

                polylineOptions = new PolylineOptions().
                        geodesic(true).
                        color(Color.parseColor("#08c7fb")).
                        width(10);
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                builder.include(route.startLocation);
                builder.include(route.endLocation);
                LatLngBounds bounds = builder.build();

                int width = getResources().getDisplayMetrics().widthPixels;
                int padding = (int) (width*0.2);
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds,padding);
                mMap.animateCamera(cameraUpdate);
                for (int i = 0; i < route.points.size(); i++) {
                    polylineOptions.add(route.points.get(i));
                }
                polylinePaths = mMap.addPolyline(polylineOptions);
            }
        }else {
            Toast.makeText(getApplicationContext(),"Aucune route n'a été trouvée",Toast.LENGTH_LONG).show();
        }
    }
}
