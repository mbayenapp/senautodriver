package com.mbayennapp.senautodriver.Model;

/**
 * Created by Mbaye on 21/10/2017.
 */

public class Demandes {
    private String adresseDepart,adresseArrivee,date,
            distance,duree,etat,gpsDepart,gpsArrivee,
            heure,lattitude,longitude,type,KEY;

    public Demandes() {
    }

    public Demandes(String adresseDepart, String adresseArrivee, String date, String distance, String duree, String etat, String gpsDepart, String gpsArrivee, String heure, String lattitude, String longitude, String type,String KEY) {
        this.adresseDepart = adresseDepart;
        this.adresseArrivee = adresseArrivee;
        this.date = date;
        this.distance = distance;
        this.duree = duree;
        this.etat = etat;
        this.gpsDepart = gpsDepart;
        this.gpsArrivee = gpsArrivee;
        this.heure = heure;
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.type = type;
        this.KEY = KEY;
    }

    public Demandes(String key, String adresseDep, String adresseArr, String gpsDep, String gpsArr, String date, String distance, String duree, String heure, String etat) {
        this.KEY = key;
        this.adresseDepart = adresseDep;
        this.adresseArrivee = adresseArr;
        this.gpsDepart = gpsDep;
        this.gpsArrivee = gpsArr;
        this.date = date;
        this.distance = distance;
        this.duree = duree;
        this.heure = heure;
        this.etat = etat;
    }

    public String getAdresseDepart() {
        return adresseDepart;
    }

    public void setAdresseDepart(String adresseDepart) {
        this.adresseDepart = adresseDepart;
    }

    public String getAdresseArrivee() {
        return adresseArrivee;
    }

    public void setAdresseArrivee(String adresseArrivee) {
        this.adresseArrivee = adresseArrivee;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getGpsDepart() {
        return gpsDepart;
    }

    public void setGpsDepart(String gpsDepart) {
        this.gpsDepart = gpsDepart;
    }

    public String getGpsArrivee() {
        return gpsArrivee;
    }

    public void setGpsArrivee(String gpsArrivee) {
        this.gpsArrivee = gpsArrivee;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKEY() {
        return KEY;
    }

    public void setKEY(String KEY) {
        this.KEY = KEY;
    }
}
